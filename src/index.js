import './css/style.scss';

const io = require('socket.io-client');

// eslint-disable-next-line no-undef
const socket = io.connect('http://127.0.0.1:3000');

socket.on('connection', () => {
  // eslint-disable-next-line no-console
  console.log('Connected to server');
});

socket.on('connect', () => {
  // eslint-disable-next-line no-console
  console.log('ok');
});

class Chat {
  constructor() {
    this.pseudo = '';
    this.el = document.querySelector('#content');
    // eslint-disable-next-line no-multi-assign
    this.app = this.el = document.querySelector('#app');
    this.run();
  }

  getRandomInt(min, max) {
    const minimum = Math.ceil(min);
    const maximum = Math.floor(max);
    return Math.floor(Math.random() * (maximum - minimum + 1) + minimum);
  }

  renderMessagesSend(message) {
    const { author, avatar, text } = message;
    const date = new Date();
    const hourminute = `${date.getHours()}:${date.getMinutes()}`;
    return `
    <div class ="row">
      <div class = "col-6 mt-2 mb-2 col-md-offset-7">
        <div class="card">
          <h5 class="card-header">
            <img width ="30px" class ="rounded-circle" src="${avatar}">
            <span>${author}</span>
          </h5>
          <div class="card-body">
            <p class="card-text">${text}</p>
            <p style="text-align:right" class="text-muted">${hourminute}</small>
          </div>
        </div>
      </div>
    </div>`;
  }

  renderMessagesReceived(message) {
    const { author, avatar, text } = message;
    const date = new Date();
    const hourminute = `${date.getHours()}:${date.getMinutes()}`;
    return ` 
    <div class ="row">
      <div class = "col-6 mb-2 ">
        <div class="card">
          <h5 class="card-header">
            <img width ="30px" class ="rounded-circle" src="${avatar}">
            <span>${author}</span>
          </h5>
        <div class="card-body">
          <p class="card-text">${text}</p>
          <p style="text-align:right" class="text-muted">${hourminute}</small>
        </div>
      </div>
    </div> `;
  }

  renderMessages() {
    return `    
      <div class = "col-md-6 col-md-offset-3 well">
      <div class ="message-content row"></div>
        ${this.renderTypingMessages()}
    </div>
    `;
  }

  renderTypingMessages() {
    return `
      <div class ="typing-message row">  
        <div class="col-9 ">
          <input type="text" class="form-control" placeholder ="Message" id="Message" aria-label="Message"/>
        </div>
        <div class='col-1'>              
          <div class="d-grid gap-2">
            <button class="btn btn-default btn-block" id="btnSubmit" onclick="Validate()">Envoyer</button>
          </div>
        </div>
      </div>`;
  }

  renderContact(contact) {
    const { avatar, author } = contact;
    return `
      <div id = "profile" class="profile mt-4" >
        <img src="${avatar}" alt="profile_picture" >
        <h5>${author}</h5>
      </div>
      <hr>
    `;
  }

  renderContacts(contacts) {
    return `
    <div class="wrapper1">
      <div class="sidebar">    
        ${contacts.map((contact) => this.renderContact(contact)).join('')}     
      </div>
    </div>
    `;
  }

  typingMessage() {
    const el = document.querySelector('.typing-message input');
    const messageuser = document.querySelector('.message-content');
    const messagebot = document.querySelector('.message-content');
    el.addEventListener('keypress', (e) => {
      const text = e.currentTarget.value;
      switch (text) {
        case 'Chuck':
        case 'chuck':
          fetch('https://api.chucknorris.io/jokes/random', {
            method: 'GET'
          })
            .then((response) => response.json())
            .then((data) => {
              const botmessage = data.value;
              const message = {
                author: 'Chuck',
                avatar: 'https://img2.freepng.fr/20180509/aiq/kisspng-chuck-norris-facts-programmer-nonstop-chuck-norris-5af2fb3a1b6329.5239792415258734661122.jpg',
                text: botmessage
              };
              const messagereactionNasa = {
                author: 'NASA',
                avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/NASA_logo.svg/2449px-NASA_logo.svg.png',
                text: ' Only Chuck Norris can win a staring contest with the eye of a hurricane  '
              };
              const messagereactionCyril = {
                author: 'Cyril',
                avatar: 'https://media-exp1.licdn.com/dms/image/C5603AQFB2qvfRVtynQ/profile-displayphoto-shrink_200_200/0/1540912243495?e=1646870400&v=beta&t=9zA7ue83b7Ofvz5ZKSpRYO04W5tbv3vd2BTbntuJtJw',
                text: 'On rigole, on rigole, on voit pas le fond du bol'
              };
              messagebot.innerHTML += this.renderMessagesReceived(message);
              messagebot.innerHTML += this.renderMessagesReceived(messagereactionCyril);
              messagebot.innerHTML += this.renderMessagesReceived(messagereactionNasa);
            });
          break;
        case 'Cyril':
        case 'cyril':
        case 'xzen':
        case 'xen':
          fetch('https://aws.random.cat/meow')
            .then((response) => response.json())
            .then((data) => {
              const botmessage = data.file;
              const message = {
                author: 'Cyril',
                avatar: 'https://media-exp1.licdn.com/dms/image/C5603AQFB2qvfRVtynQ/profile-displayphoto-shrink_200_200/0/1540912243495?e=1646870400&v=beta&t=9zA7ue83b7Ofvz5ZKSpRYO04W5tbv3vd2BTbntuJtJw',
                text: `<div class ="message-content-bot row"><img src="${botmessage}" /></div>`
              };
              const messagereactionNasa = {
                author: 'NASA',
                avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/NASA_logo.svg/2449px-NASA_logo.svg.png',
                text: 'Connaissez vous la chatte <a href="https://sciencepost.fr/chat-espace-france/"  </a> C 341 ?  '
              };
              const messagereactionChuck = {
                author: 'Chuck',
                avatar: 'https://img2.freepng.fr/20180509/aiq/kisspng-chuck-norris-facts-programmer-nonstop-chuck-norris-5af2fb3a1b6329.5239792415258734661122.jpg',
                text: '<div class ="message-content-bot row"><img src="https://media0.giphy.com/media/BIuuwHRNKs15C/giphy.gif?cid=ecf05e477fx4zcgvskoavwewojda3bkw30vota0kdpuuzh41&rid=giphy.gif&ct=g"/></div>'
              };
              messagebot.innerHTML += this.renderMessagesReceived(message);
              messagebot.innerHTML += this.renderMessagesReceived(messagereactionNasa);
              messagebot.innerHTML += this.renderMessagesReceived(messagereactionChuck);
            });
          break;
        case 'NASA':
        case 'Nasa':
        case 'nasa': {
          let url = 'https://api.nasa.gov/planetary/apod?api_key=nGyb4L9GhjsBPfcA5Z4q5LzxUzs5ryYDMWIQA4kE';
          const randomDay = this.getRandomInt('01', '30');
          const randomMonth = this.getRandomInt('01', '12');
          const randomYear = this.getRandomInt('2000', '2021');
          const newdate = `${randomYear}-${randomMonth}-${randomDay}`;
          const frenchdate = `${randomDay}-${randomMonth}-${randomYear}`;
          url += `&date=${newdate}`;
          fetch(url)
            .then((response) => response.json())
            .then((data) => {
              const botmessage = data.url;
              const botdescription = data.explanation;
              const message = {
                author: 'NASA',
                avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/NASA_logo.svg/2449px-NASA_logo.svg.png',
                text: `<div class ="message-content-bot row"><img src="${botmessage}" /><br> Photo prise le : ${frenchdate}<br><br>${botdescription} </div>`
              };
              const messagereactionCyril = {
                author: 'Cyril',
                avatar: 'https://media-exp1.licdn.com/dms/image/C5603AQFB2qvfRVtynQ/profile-displayphoto-shrink_200_200/0/1540912243495?e=1646870400&v=beta&t=9zA7ue83b7Ofvz5ZKSpRYO04W5tbv3vd2BTbntuJtJw',
                text: 'Mouais, je préfère les chats'
              };
              messagebot.innerHTML += this.renderMessagesReceived(message);
              messagebot.innerHTML += this.renderMessagesReceived(messagereactionCyril);
            });
          break;
        }
        case 'Help':
        case 'help': {
          const url = 'https://api.giphy.com/v1/gifs/random?api_key=PC9DJk3HROykezV9gWHhT8sDc18sLZTS&tag=help&rating=g';
          fetch(url)
            .then((response) => response.json())
            .then((json) => {
              const botmessage = json.data.images.original.url;
              const message = {
                author: 'Help',
                avatar: 'https://media.istockphoto.com/vectors/call-center-help-customer-service-logo-support-and-contact-icon-agent-vector-id1163967622?k=20&m=1163967622&s=170667a&w=0&h=d7OAbqTOK3txeIAVBdOGqqTO5WT5f2DOVQZjAnvNSLI=',
                text: `<div class ="message-content-bot row"> <img src="${botmessage}" /> <br><br> Bienvenue sur le chatbot.<br><br> 
                - Help : Bah comme vous pouvez le voir, je vous donne les commandes en affichant un joli GIF au hasard. Je suis lié à L'API GIPHY <br> <br>
                - NASA : Affichera une photo prise par la nasa au hasard en les années 2000 et 2021<br> <br> <br>
                - Chuck : Affichera une petite blague Chuck Norris en anglais. Rire en apprennant, merveilleux n'est ce pas?. <br> <br>
                - Cyril : Ce bot affichera une image de chat au hasard. Non ce n'est pas une tentative de corruption. <br> <br>
                Quand vous appellerez un bot, les autres réagiront avec un commentaire prédéfini. </div>`
              };
              messagebot.innerHTML += this.renderMessagesReceived(message);
            });
          break;
        }
        default:
          break;
      }
      if (e.keyCode === 13) {
        const message = {
          author: this.pseudo,
          avatar: 'https://previews.123rf.com/images/tuktukdesign/tuktukdesign1608/tuktukdesign160800057/61010892-ic%C3%B4ne-d-utilisateur-homme-profil-homme-d-affaires-avatar-illustration-vectorielle-de-glyphe-de-perso.jpg',
          text
        };
        messageuser.innerHTML += this.renderMessagesSend(message);
        e.currentTarget.value = '';
        socket.emit('usermessage', message);
      }
    });
  }

  run() {
    const contacts = [{
      author: 'Help',
      avatar: 'https://media.istockphoto.com/vectors/call-center-help-customer-service-logo-support-and-contact-icon-agent-vector-id1163967622?k=20&m=1163967622&s=170667a&w=0&h=d7OAbqTOK3txeIAVBdOGqqTO5WT5f2DOVQZjAnvNSLI='
    }, {
      author: 'NASA',
      avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/NASA_logo.svg/2449px-NASA_logo.svg.png'
    }, {
      author: 'Chuck',
      avatar: 'https://img2.freepng.fr/20180509/aiq/kisspng-chuck-norris-facts-programmer-nonstop-chuck-norris-5af2fb3a1b6329.5239792415258734661122.jpg'
    }, {
      author: 'Cyril',
      avatar: 'https://media-exp1.licdn.com/dms/image/C5603AQFB2qvfRVtynQ/profile-displayphoto-shrink_200_200/0/1540912243495?e=1646870400&v=beta&t=9zA7ue83b7Ofvz5ZKSpRYO04W5tbv3vd2BTbntuJtJw'
    }];
    this.el.innerHTML += this.renderContacts(contacts);
    this.el.innerHTML += this.renderMessages();
    socket.on('connection', (id) => {
      this.pseudo = id;
    });
    socket.on('messagesent', (message) => {
      this.el.innerHTML += this.renderMessagesReceived(message);
    });
    this.typingMessage();
  }
}

export default new Chat();
