# Chatbot-io
Chatbot for talking with minimum 3 bots in one canal. Every bot can make minimum 3 actions and all bots have one command in common.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
Install node.js 
```bash
sudo apt install nodejs
```
Check if you have the latest version of node 
```bash
nodejs -v
```
Install npm
```bash
sudo apt install npm
```
### Installation

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

Use node v16 use :
```bash
nvm install 16
```

```bash
npm i
```

## Usage

Start the application dev with :

### Launch Socket.io server
```bash
cd server
node server.js
```
### Launch project
```bash
npm run start
```
## Bots
In this project, by default 3 bots is generating with differents commands.

#### :rocket: Nasa
Connected to **Nasa** API.
| name    | result                                           | cmds                                      |
|---------|--------------------------------------------------|-------------------------------------------|
| Nasa    | Nasa respond with random picture                 | ['nasa', 'Nasa']                          |
| Cyril   | Cyril respond with a message                     |                                           |

#### :black_joker: Chuck Norris
Connected to **Chuck Norris** API.
| name            | result                                           | cmds                                      |
|-----------------|--------------------------------------------------|-------------------------------------------|
| Chuck Norris    | Bot respond with Chuck Norris Joke               | ['Chuck', 'chuck']                        |
| Cyril           | Cyril respond with a message                     |                                           |
| Nasa            | Nasa respond with a message                      |                                           |

#### :cat2: Cyril
Connected to **Cat** API.
| name            | result                                           | cmds                                      |
|-----------------|--------------------------------------------------|-------------------------------------------|
| Cyril           | Bot respond with a cat picture                   | ['Cyril', 'cyril', 'xen', 'xzen']         |
| Chuck Norris    | Bot respond with Chuck Norris GIF                |                                           |
| Nasa            | Nasa respond with a link artice about a cat      |                                           |

#### :sos: Help
Connected to **GIPHY** API.
| name    | result                                           | cmds                                      |
|---------|--------------------------------------------------|-------------------------------------------|
| Help    | Bot respond with all commands possible           | ['Help', 'help']           |

## Built With

* [Server Socket.io](https://socket.io/docs/v4/server-installation/) - the server
* [Client Socket.io](https://socket.io/docs/v4/client-installation/) - the client

## Versioning
This is the 1.0.0 version

## Authors

* **Akalmie** - *Initial work* - :ocean: [Akalmie](https://gitlab.com/Akalmie)

* **Xen** - *Webpack environement* - :smile_cat: [Xen](https://gitlab.com/xzen769)

## Acknowledgments

* Thanks to :alien: [Maengdok](https://gitlab.com/Maengdok) & :gem: [Rubis](https://gitlab.com/remirubis) for their help.

